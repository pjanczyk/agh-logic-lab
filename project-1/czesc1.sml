(*
 * 2017  Piotr Janczyk
 *
 * Zadanie domowe 1, czesc 1
 *  structure file
 *)
structure id291444 :> PART_ONE =
struct
  exception NotImplemented

  datatype 'a tree = Leaf of 'a | Node of 'a tree * 'a * 'a tree

  fun sum n =
    if n = 1 then 1
	else n + sum (n - 1)
	
  fun fac n =
    if n = 1 then 1
	else n * fac (n - 1)

  fun fib n = 
    if n <= 1 then 1 
    else fib (n - 1) + fib (n - 2)

  fun gcd (a, b) =
    if b = 0 then a
	else gcd (b, a mod b)
 
  fun max [] = 0
    | max (head::tail) = 
        let
		  val maxTail = max tail
		in
		  if head >= maxTail then head
		  else maxTail
		end

  fun sumTree t = 
	case t of 
	  Leaf v => v
    | Node (l, v, r) => (sumTree l) + v + (sumTree r)
	
  fun depth t =
    case t of
	  Leaf _ => 0
	| Node (l, _, r) => (max [depth l, depth r]) + 1
  
  fun binSearch t x =
    case t of
	  Leaf v => v = x
	| Node (l, v, r) =>
	    if x < v then binSearch l x
		else if x > v then binSearch r x
		else true
	
  fun preorder t =
    case t of
	  Leaf v => [v]
	| Node (l, v, r) => v :: (preorder l) @ (preorder r)

  fun listAdd [] l2 = l2
    | listAdd l1 [] = l1
	| listAdd (head1::tail1) (head2::tail2) =
        (head1 + head2) :: (listAdd tail1 tail2)

  fun insert m [] = [m]
    | insert m (head::tail) =
	    if m <= head then m :: head :: tail
		else head :: (insert m tail)
  
  fun insort [] = []
    | insort (head::tail) = insert head (insort tail)

  fun compose f g = g o f
  
  fun curry f a b = f (a, b)
  
  fun uncurry f (a, b) = f a b
  
  fun multifun f n =
    if n = 1 then f
	else f o (multifun f (n - 1))

  fun ltake _ 0 = []
    | ltake [] _ = []
	| ltake (head::tail) n = head :: (ltake tail (n - 1))
	
  fun lall f [] = true
    | lall f (head::tail) =
        if not (f head) then false
        else lall f tail
  
  fun lmap f [] = []
    | lmap f (head::tail) = (f head) :: (lmap f tail)

  fun lrev [] = []
    | lrev (head::tail) = (lrev tail) @ [head]
  
  fun lzip ([], _) = []
    | lzip (_, []) = []
    | lzip (head1::tail1, head2::tail2) = (head1, head2) :: (lzip (tail1, tail2))
	
  fun split [] = ([], [])
    | split (head::tail) =
	    let
		  val (a, b) = split tail
		in
		  (head :: b, a)
	    end

  fun cartprod [] _ = []
    | cartprod _ [] = []
	| cartprod (head1::tail1) (head2::tail2) =
	    (head1, head2) :: (cartprod [head1] tail2) @ (cartprod tail1 (head2::tail2))

end
