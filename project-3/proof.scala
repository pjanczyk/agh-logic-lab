/**
  (c) 2017  Piotr Janczyk
 */

object Main {

  type /\[T1, T2] = (T1, T2)
  type \/[T1, T2] = Either[T1, T2]

  trait Proofs {
    type A
    type B
    type C
    type D

    // impl_rozdz : (A -> B) -> (A -> C) -> A -> B -> C.
    val impl_rozdz: (A => B) => (A => C) => A => B => C =
      a_impl_b => a_impl_c => a => b => a_impl_c(a)

    // impl_komp : (A -> B) -> (B -> C) -> A -> C.
    val impl_komp: (A => B) => (B => C) => A => C =
      a_impl_b => b_impl_c => a => b_impl_c(a_impl_b(a))

    // impl_perm : (A -> B -> C) -> B -> A -> C.
    val impl_perm: (A => B => C) => B => A => C =
      a_impl_b_impl_c => b => a => a_impl_b_impl_c(a)(b)

    // impl_conj : A -> B -> A /\ B.
    val impl_conj: A => B => A /\ B =
      a => b => (a, b)

    // conj_elim_l : A /\ B -> A.
    val conj_elim_1: A /\ B => A =
      a_and_b => a_and_b._1

    // disj_intro_l : A -> A \/ B.
    val disj_intro_1: A => A \/ B =
      a => Left(a)

    // rozl_elim : A \/ B -> (A -> C) -> (B -> C) -> C.
    val rozl_elim: A \/ B => (A => C) => (B => C) => C =
      a_or_b => a_impl_c => b_impl_c => a_or_b match {
        case Left(a) => a_impl_c(a)
        case Right(b) => b_impl_c(b)
      }

    // diamencik : (A -> B) -> (A -> C) -> (B -> C -> D) -> A -> D.
    val diamencik: (A => B) => (A => C) => (B => C => D) => A => D =
      a_impl_b => a_impl_c => b_impl_c_impl_d => a => b_impl_c_impl_d(a_impl_b(a))(a_impl_c(a))

    // slaby_peirce : ((((A -> B) -> A) -> A) -> B) -> B.
    val slaby_peirce: ((((A => B) => A) => A) => B) => B =
      h => h(i => i(j => h(k => j)))

    // rozl_impl_rozdz : (A \/ B -> C) -> (A -> C) /\ (B -> C).
    val rozl_impl_rozdz: (A \/ B => C) => (A => C) /\ (B => C) =
      a_or_b_impl_c => (a => a_or_b_impl_c(Left(a)), b => a_or_b_impl_c(Right(b)))

    // rozl_impl_rozdz_odw : (A -> C) /\ (B -> C) -> A \/ B -> C.
    val rozl_impl_rozdz_odw: (A => C) /\ (B => C) => A \/ B => C =
      a_impl_c_and_b_impl_c => a_or_b => a_or_b match {
        case Left(a) => a_impl_c_and_b_impl_c._1(a)
        case Right(b) => a_impl_c_and_b_impl_c._2(b)
      }

    // curry : (A /\ B -> C) -> A -> B -> C.
    val curry: (A /\ B => C) => A => B => C =
      a_and_b_impl_c => a => b => a_and_b_impl_c((a, b))

    // uncurry : (A -> B -> C) -> A /\ B -> C.
    val uncurry: (A => B => C) => A /\ B => C =
      a_impl_b_impl_c => a_and_b => a_impl_b_impl_c(a_and_b._1)(a_and_b._2)
  }


  def main(args: Array[String]): Unit = {
    println("OK")
  }
}
