(*
 * 2017  Piotr Janczyk
 *
 * Testy implementacji funkcji zawartych w pliku "czesc1.sml".
 * Uruchamianie: $ sml tests.sml
 *)

CM.make "sources.cm";
open id291444;

fun check_rec [] _ = ()
  | check_rec (head::tail) n = (
      print (Int.toString n);
	  print (if head then ". OK\n" else ". ERROR\n");
	  check_rec tail (n + 1)
	);

fun check l = check_rec l 1;


check [
  55 = sum 10,
  1 = sum 1,
  1 = fac 1,
  24 = fac 4,
  1 = fib 0,
  1 = fib 1,
  8 = fib 5,
  3 = gcd (3, 0),
  5 = gcd (15, 35),
  0 = max [],
  7 = max [5,3,6,7,4],
  3 = sumTree (Leaf 3),
  17 = sumTree (Node (Node (Leaf 1, 3, Leaf 2), 7, Leaf 4)),
  0 = depth (Leaf 1),
  2 = depth (Node (Node (Leaf 1, 3, Leaf 2), 7, Leaf 4)),
  true = binSearch (Node (Node (Leaf 1, 2, Leaf 3), 4, Leaf 7)) 2,
  false = binSearch (Node (Node (Leaf 1, 2, Leaf 3), 4, Leaf 7)) 5,
  [7, 3, 1, 2, 4] = preorder (Node (Node (Leaf 1, 3, Leaf 2), 7, Leaf 4)),
  [4, 6, 5] = listAdd [1, 2] [3, 4, 5],
  [1, 2, 3, 4, 5] = insert 3 [1, 2, 4, 5],
  [1, 2, 3, 5, 7] = insort [3, 7, 5, 1, 2],
  12 = (compose (fn x => x + 1) (fn x => 2 * x)) 5,
  6 = (curry (fn (a,b) => a * b)) 2 3,
  6 = (uncurry (fn a => fn b => a * b)) (2, 3),
  4 = (multifun (fn x => x + 1) 3) 1,
  256 = (multifun (fn x => x * x) 3) 2,
  [3,7,5] = ltake [3, 7, 5, 1, 2] 3,
  [3,7,5,1,2] = ltake [3, 7, 5, 1, 2] 7,
  true = lall (fn x => x > 0) [1, 2, 3],
  false = lall (fn x => x > 0) [1, 2, 0, 3],
  [2, 3, 4] = lmap (fn x => x + 1) [1, 2, 3],
  [4, 3, 2, 1] =  lrev [1, 2, 3, 4],
  [("A", 8), ("B", 13), ("C", 18)] = lzip (["A","B","C"],[8,13,18,7,10,12]),
  ([], []) = split [],
  ([1], []) = split [1],
  ([1, 5, 9], [3, 7, 11]) = split [1, 3, 5, 7, 9, 11],
  [] = cartprod [1, 2] [],
  [(1,3), (1,4), (1,5), (2,3), (2,4), (2,5)] = cartprod [1, 2] [3, 4, 5]
];

OS.Process.exit OS.Process.success;
